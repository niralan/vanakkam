package org.ct.learn.html;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HtmlformreceiverApplication {

	public static void main(String[] args) {
		SpringApplication.run(HtmlformreceiverApplication.class, args);
	}

}
